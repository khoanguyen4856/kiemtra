﻿namespace KiemTra.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
