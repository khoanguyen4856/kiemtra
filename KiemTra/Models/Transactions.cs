﻿namespace KiemTra.Models
{
    public class Transactions
    {
        public int TransactionalId { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
    }
}
