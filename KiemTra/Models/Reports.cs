﻿namespace KiemTra.Models
{
    public class Reports
    {
        public int ReportId { get; set; }
        public int AccountId { get; set; }
        public int LogsId { get; set;}
        public int TransactionId { get; set;}
        public string Reportname { get; set; }
        public DateTime Reportdate { get; set; } 
    }
}
