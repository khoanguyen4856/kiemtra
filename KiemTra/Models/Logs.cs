﻿\namespace KiemTra.Models
{
    public class Logs
    {
        public int LogsId { get; set; }
        public int TransactionalId { get; set; }
        public DateTime Logindate { get; set; }
        public TimeSpan Logintime { get; set; }


    }
}
